﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishLibraryUniversal
{
    public static class UserFunctions
    {
        public delegate  double FuncTemplate(params double[] coords);
        private static List<Tuple<FuncTemplate,int,string>> FunctionList;
        static void InitFunctons()
        {
            FunctionList = new List<Tuple<FuncTemplate, int,string>>();
            FunctionList.Add(new Tuple<FuncTemplate,int, string>(x=>x[0]*(10-x[0]),1, "y=x*(10-x)"));
            FunctionList.Add(new Tuple<FuncTemplate, int, string>(x => -0.1 * (x[0] * x[0] + x[1] * x[1]), 2, "y=-0,1*(x^2+z^2)"));
            FunctionList.Add(new Tuple<FuncTemplate, int, string>(x => 10 * (Math.Sin(0.1 * x[0]) + Math.Sin(0.1 * x[1])), 2, "y=10*(sin(0,1x)+sin(0,1z))"));
            FunctionList.Add(new Tuple<FuncTemplate, int, string>(x => 10 * (Math.Sin(0.1 * x[0]) + Math.Sin(0.1 * x[1])+Math.Sin(0.1*x[2])), 3, "y=10*(sin(0,1a)+sin(0,1b)+sin(0,1c))"));
            FunctionList.Add(new Tuple<FuncTemplate, int, string>(x => -0.1 * (x[0] * x[0] + x[1] * x[1] + x[2] * x[2] + x[3] * x[3]) + 100, 4, "y=-0,1*(a^2+b^2+c^2+d^2)+100"));

        }
         public static void ShowFunctionList()
         {
             InitFunctons();
             for (int i = 0; i < FunctionList.Count; i++)
                 Console.WriteLine("{0}. {1}", i, FunctionList[i].Item3);
         }
        /// <summary>
        /// Выбор функции по его номеру
        /// </summary>
        /// <param name="number">Номер функции</param>
        /// <returns>Возвращает кортеж, состоящий из функции и ее количества аргументов. Если такой функции нет, то возращается null</returns>
         public static Tuple<FuncTemplate,int> SelectFunction(int number)
         {
             if (number >= FunctionList.Count||number<0) return null;
             return new Tuple<FuncTemplate, int>(FunctionList[number].Item1, FunctionList[number].Item2);
         }
    }
}
